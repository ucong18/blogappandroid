package com.ucong.model;

public class LoginResult {
    private boolean error;
    private String messages;
    private User user;

    public LoginResult(boolean error, String messages, User user) {
        this.error = error;
        this.messages = messages;
        this.user = user;
    }

    public boolean isError() {
        return error;
    }

    public String getMessages() {
        return messages;
    }

    public User getUser() {
        return user;
    }
}
