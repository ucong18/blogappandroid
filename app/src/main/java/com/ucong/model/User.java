package com.ucong.model;

public class User {
    private String token;
    private String name;
    private String email;
    private String password;

    private User(String email){
        this.email = email;
    }

    public User(String token, String name, String email) {
        this.token = token;
        this.name = name;
        this.email = email;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
