package com.ucong.apihelper;

public class UtilsApi {


    public static String BASE_URL_API = "http://10.0.2.2:8000/api/";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiServices getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiServices.class);
    }
}
