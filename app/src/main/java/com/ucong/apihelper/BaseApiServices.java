package com.ucong.apihelper;

import com.ucong.model.LoginResult;
import com.ucong.model.RegisterResult;

import javax.xml.transform.Result;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface BaseApiServices {

    // call API http://localhost:8000/api/login
    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> userlogin(
                            @Field("email") String email,
                            @Field("password") String password);

    // call API http://localhost:8000/api/register
    @FormUrlEncoded
    @POST("register")
    Call<ResponseBody> createUser(
                        @Field("name") String nama,
                        @Field("email") String email,
                        @Field("password") String password,
                        @Field("c_password") String c_password);

    // call API http://localhost:8000/api/logout
    @FormUrlEncoded
    @GET("logout")
    Call<ResponseBody> userLogout();

}
