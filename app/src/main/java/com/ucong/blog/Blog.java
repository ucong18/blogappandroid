package com.ucong.blog;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

import com.ucong.apihelper.BaseApiServices;
import com.ucong.apihelper.UtilsApi;

import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Blog extends AppCompatActivity {
    private Toolbar toolbar;
    private ImageView toolbarTitle;
    private Menu menuLogout;
    private Button btnLogout;
    BaseApiServices mApiService;
    ArrayAdapter<CharSequence> adapter;
    ListView listView;

    String[] listlanguange = {"JAVA","KOTLIN","PHP","IOS",".NET"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);

        //bind view
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbarTitle = (ImageView) findViewById(R.id.toolbar_title);
        menuLogout = (Menu)findViewById(R.id.logout);

        mApiService = UtilsApi.getAPIService();

        //set toolbar
        setSupportActionBar(toolbar);

        //menghilangkan titlebar bawaan
        if(toolbar != null){
            getSupportActionBar().setDisplayShowCustomEnabled(false);
        }



        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.activity_listview, listlanguange);
        ListView listView = (ListView)findViewById(R.id.languangelist);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.blogmenu, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.home){
            startActivity(new Intent(Blog.this, MainActivity.class));
        } else if (item.getItemId()==R.id.help){

        } else if (item.getItemId() == R.id.logout) {
            logout();
        }
        return true;
    }

    public void logout() {
        SessionManager.setLoggedIn(getApplicationContext(), false);
        mApiService.userLogout();
        startActivity(new Intent(Blog.this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();

    }



}
