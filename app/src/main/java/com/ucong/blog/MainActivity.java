package com.ucong.blog;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import android.content.SharedPreferences;

public class MainActivity extends AppCompatActivity {
    protected Toolbar toolbar;
    protected ImageView toolbarTitle;

    SharedPreferences sharedpreferences;

    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        toolbar =   findViewById(R.id.toolbar_main);
        toolbarTitle = findViewById(R.id.toolbar_title);


        setSupportActionBar(toolbar);


        if(toolbar != null){
            getSupportActionBar().setDisplayShowCustomEnabled(false);
        }

        Button btntes = findViewById(R.id.buttontes);

        btntes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Test Button", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        if(SessionManager.getLoggedStatus(getApplicationContext())){
            inflater.inflate(R.menu.blogmenu,menu);
            menu.removeItem(R.id.home);

        }else {
            inflater.inflate(R.menu.homemenu, menu);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.login){
            startActivity(new Intent(this, LoginActivity.class));
        } else if (item.getItemId() == R.id.register){
            startActivity(new Intent(this, RegisterUserActivity.class));
        } else if (item.getItemId() == R.id.about) {
            startActivity(new Intent(this, About.class));
        } else if (item.getItemId() == R.id.logout) {
            logout();
        }else if (item.getItemId() == R.id.help) {
            return true;
        }

        return true;
    }

    public void logout() {
        SessionManager.setLoggedIn(getApplicationContext(), false);
        startActivity(new Intent(this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
}
