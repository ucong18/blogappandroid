package com.ucong.blog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;


import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import com.ucong.apihelper.BaseApiServices;
import com.ucong.apihelper.UtilsApi;
import com.ucong.blog.SessionManager;
import com.ucong.model.User;

import org.json.JSONException;
import org.json.JSONObject;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity{

    private EditText editTextEmail;
    private EditText editTextPassword;
    protected Button btnLogin;
    protected TextView btnRegister;
    protected CheckBox btnShowPassword;
    protected RelativeLayout loginForm;

    BaseApiServices mApiService;
    ProgressDialog loading;
    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        mApiService = UtilsApi.getAPIService();

        editTextEmail =  findViewById(R.id.editEmail);
        editTextPassword =  findViewById(R.id.editPassword);
        btnLogin =  findViewById(R.id.buttonLogin);
        btnRegister = findViewById(R.id.textViewRegister);
        btnShowPassword = findViewById(R.id.showPassword);

        loginForm = findViewById(R.id.loginForm);


        editTextEmail.setText("ucong@gmail.com");
        editTextPassword.setText("ilkom741");

        // Check if UserResponse is Already Logged In
        if(SessionManager.getLoggedStatus(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), Blog.class);
            startActivity(intent);
            finish();
        } else {
            loginForm.setVisibility(View.VISIBLE);
        }


        btnShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btnShowPassword.isChecked()){
                    editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmail.getText().toString().trim();
                final String password = editTextPassword.getText().toString().trim();

                if (email.isEmpty()) {
                    editTextEmail.setError("Email is required");
                    editTextEmail.requestFocus();
                    return;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    editTextEmail.setError("Enter a valid email");
                    editTextEmail.requestFocus();
                    return;
                }

                if (password.isEmpty()) {
                    editTextPassword.setError("Password is required");
                    editTextPassword.requestFocus();
                    return;
                }

                if (password.length() < 6) {
                    editTextPassword.setError("Password Should be atleast 6 character long");
                    editTextPassword.requestFocus();
                    return;
                }

                mApiService.userlogin(email, password)
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()){
                                    Log.i("debug", "onResponse: Success");
                                    try {
                                        Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                        
                                        startActivity(new Intent(LoginActivity.this, Blog.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//                                        SessionManager.setLoggedIn(getApplicationContext(), true);
                                        finish();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Credentials are not Valid.",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e("debug", "onFailure: ERROR > " + t.toString());
                                Toast.makeText(LoginActivity.this, "Test Failed Link", Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterUserActivity.class);
                startActivity(intent);

            }
        });

    }
}
