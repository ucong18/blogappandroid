package com.ucong.blog;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Context;

import android.util.Log;
import android.util.Patterns;



import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonIOException;
import com.ucong.apihelper.BaseApiServices;
import com.ucong.apihelper.UtilsApi;
//import com.ucong.model.RegisterResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.ucong.model.User;

public class RegisterUserActivity extends AppCompatActivity {

    private EditText editTextNewName;
    private EditText editTextNewEmail;
    private EditText editTextNewPassword;
    private EditText editTextNewCPassword;

    BaseApiServices mApiService;
    ProgressDialog loading;
    Context mContext;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        mApiService = UtilsApi.getAPIService();

        editTextNewName      = findViewById(R.id.editNewName);
        editTextNewEmail     =findViewById(R.id.editNewEmail);
        editTextNewPassword  =findViewById(R.id.editNewPassword);
        editTextNewCPassword =findViewById(R.id.editNewCPassword);

        TextView buttonLogin =  findViewById(R.id.textViewLogin);
        Button buttonRegister = findViewById(R.id.buttonRegister);


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextNewName.getText().toString().trim();
                String email = editTextNewEmail.getText().toString().trim();
                String password = editTextNewPassword.getText().toString().trim();
                String cpassword = editTextNewCPassword.getText().toString().trim();

                if (name.isEmpty()) {
                    editTextNewName.setError("Email is required");
                    editTextNewName.requestFocus();
                    return;
                }

                if (email.isEmpty()) {
                    editTextNewEmail.setError("Email is required");
                    editTextNewEmail.requestFocus();
                    return;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    editTextNewEmail.setError("Enter a valid email");
                    editTextNewEmail.requestFocus();
                    return;
                }

                if (password.isEmpty()) {
                    editTextNewPassword.setError("Password is required");
                    editTextNewPassword.requestFocus();
                    return;
                }

                if (cpassword.isEmpty()) {
                    editTextNewCPassword.setError("Password is required");
                    editTextNewCPassword.requestFocus();
                    return;
                }

                if (password.length() < 8 ) {
                    editTextNewPassword.setError("Password Should be atleast 6 character long");
                    editTextNewPassword.requestFocus();
                    return;
                }

                if (cpassword.length() < 8 ) {
                    editTextNewCPassword.setError("Password Should be atleast 6 character long");
                    editTextNewCPassword.requestFocus();
                    return;
                }


                mApiService.createUser(name,email,password,cpassword)
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if(response.isSuccessful()){
                                    Log.i("debug", "onResponse: Success");
                                    loading.dismiss();
                                    try {
                                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                        if (jsonRESULTS.getString("success").equals("true")){
                                            Toast.makeText(mContext, "Register Success", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(mContext, LoginActivity.class));
                                        } else {
                                            String error_message = jsonRESULTS.getString("error_msg");
                                            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                                        }

                                    }catch (JsonIOException e){
                                        e.printStackTrace();
                                    }catch (IOException e){
                                        e.printStackTrace();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }else{
                                    Toast.makeText(getApplicationContext(),"Register Failed", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e("debug", "onFailure: ERROR > " + t.toString());

                            }
                        });
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterUserActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
